#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- 
# -- 
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Email lmay@lmaye.com
# Date: 2018/5/5 13:48 星期六
# ----------------------------------------------------------
import sys
from core.utils import sftp_config
from core.utils.sftp_utils import sftp_connect, sftp_upload, sftp_download

if __name__ == '__main__':
    result = sftp_connect(host=sftp_config.host, port=sftp_config.port, username=sftp_config.username,
                          password=sftp_config.password)
    if result[0] != 1:
        print(result[1])
        sys.exit()
    else:
        print("connection success")
    ftp_connect = result[2]
    # 上传
    upload = sftp_upload(ftp_connect, sftp_config.localDir, sftp_config.homeDir)
    print("----------->>> {}".format(upload[1]))
    # 下载
    # download = sftp_download(ftp_connect, sftp_config.localDir + "/download", sftp_config.homeDir)
    # print("----------->>> {}".format(download[1]))
    ftp_connect.close()

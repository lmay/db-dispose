### 关于项目
        此项目为`Python`数据同步和数据生成客户端，客户端仅仅显示数据库执行日志。
    首先配置程序参数`db-dispose.yml`，然后配置同步的SQL语句，最后在客户端界面
    点击`执行`即可看到数据库同步状态。
        docs目录下存放一些支持工具和测试脚本，详细操作见resources/db里面有基本
    的配置信息。resources/db目录下的.yml文件是数据库配置文件, .json是sql语句
    文件。
    
    ※ oracle-to-mysql.json解析:
    {
      "input_db": {
        "db1": {
          "tb_name": "member_backups",
          "normal_sql": "insert into member_backups (id, type, client_source, email, mobile, password, name, cert_type, cert_no, address, created_at) value (%s, %s, 'WEB', %s, %s, %s, %s, %s, %s, %s, %s)"
        },
        "db2": {
          "tb_name": "member_backups",
          "normal_sql": "insert into member_backups (id, type, client_source, email, mobile, password, name, cert_type, cert_no, address, created_at) value (%s, %s, 'WEB', %s, %s, %s, %s, %s, %s, %s, %s)"
        }
      },
      "output_db": {
        "db1": {
          "tb_name": "cmember",
          "normal_sql": "select ccode, customer_type, email, telphone, tradepass, account_holders, legal_representativeid, businesscode, adress, open_date from cmember",
          "page_sql": "select ccode, customer_type, email, telphone, tradepass, account_holders, legal_representativeid, businesscode, adress, open_date from (select rownum rn, t.* from cmember t where rownum <= {}) cmember where cmember.rn > {}",
          "where": "usrcustid is not null",
          "special_dispose": "if k=='CUSTOMER_TYPE':\n\tif 1==v:\n\t\tli.append(2)\n\telif 2==v:\n\t\tli.append(1)\n\telse:\n\t\tli.append(None)\nelif k=='LEGAL_REPRESENTATIVEID':\n\tif 1==li[1]:\n\t\tli.append(15)\n\t\tli.append(v)\nelif k=='BUSINESSCODE':\n\tif 2==li[1]:\n\t\tli.append(20)\n\t\tli.append(v)\nelse:\n\tli.append(v)"
        },
        "db2": {
          "tb_name": "cmember",
          "normal_sql": "select ccode, customer_type, email, telphone, tradepass, account_holders, legal_representativeid, businesscode, adress, open_date from cmember",
          "page_sql": "select ccode, customer_type, email, telphone, tradepass, account_holders, legal_representativeid, businesscode, adress, open_date from (select rownum rn, t.* from cmember t where rownum <= {}) cmember where cmember.rn > {}",
          "where": "usrcustid is not null",
          "special_dispose": "if k=='CUSTOMER_TYPE':\n\tif 1==v:\n\t\tli.append(2)\n\telif 2==v:\n\t\tli.append(1)\n\telse:\n\t\tli.append(None)\nelif k=='LEGAL_REPRESENTATIVEID':\n\tif 1==li[1]:\n\t\tli.append(15)\n\t\tli.append(v)\nelif k=='BUSINESSCODE':\n\tif 2==li[1]:\n\t\tli.append(20)\n\t\tli.append(v)\nelse:\n\tli.append(v)"
        }
      }
    }
    
    多表示例:
    {
      "output_db": {
        "db1": {
          "tb_name": "account_open_record a, account_bank_card_bind_record b",
          "normal_sql": "select a.member_id, a.name, a.cert_type, a.cert_no, a.mobile, a.bank_card_no, b.bank_mobile from account_open_record a, account_bank_card_bind_record b",
          "page_sql": "select a.member_id, a.name, a.cert_type, a.cert_no, a.mobile, a.bank_card_no, b.bank_mobile, 0 from account_open_record a, account_bank_card_bind_record b {} limit {}, {}",
          "where": "a.member_id = b.member_id and a.name != ''",
          "special_dispose": ""
        }
      }
    }
    
    ※ 更新示例:
    -- 更新缺陷:
    --      Python数据库更新事务非常慢(pymysql支持批量插入效率较高，但是update则仅仅支持单条，处理非常慢。)不建议使用此方案！
    -- 解决方案:
    --      在导入库中创建临时表，然后通过SQL脚本处理;
    {
      "input_db": {
        "db6": {
          "tb_name": "asset",
          "normal_sql": "update asset set paid_principal = %s, paid_interest = %s where asset_no = %s"
        }
      },
      "output_db": {
        "db6": {
          "tb_name": "PRODUCT",
          "normal_sql": "select t1.amount, t1.interest, t2.PRODUCT_CODE from (SELECT a.PRODUCTID, SUM(a.amount) amount, SUM(interest) interest FROM INSTALMENTS a WHERE a.state = 1 GROUP BY a.PRODUCTID) t1, PRODUCT t2 where t1.PRODUCTID = t2.ID",
          "page_sql": "select amount, interest, PRODUCT_CODE from (select ROWNUM n, t.* from (select t1.amount, t1.interest, t2.PRODUCT_CODE from (SELECT a.PRODUCTID, SUM(a.amount) amount, SUM(interest) interest FROM INSTALMENTS a WHERE a.state = 1 GROUP BY a.PRODUCTID) t1, PRODUCT t2 where t1.PRODUCTID = t2.ID) t where ROWNUM <= {}) temp where n > {}",
          "where": "",
          "special_dispose": "if k == 'AMOUNT' or k == 'INTEREST':\n\tif v:\n\t\tli.append(round(v * 100))\n\telse:\n\t\tli.append(0)\nelse:\n\tli.append(v)"
        }
      }
    }
    
    1. input_db         数据导入
    2. output_db        数据导出
        db1, db2        对应导出库的Key(必须一致)
        tb_name         数据库表名
        normal_sql      正常的SQL
        page_sql        分页的SQL
        where           查询条件
        special_dispose 特殊字段处理表达式
        
        special_dispose示例:(\n\t格式化表达式)
        if k == 'CUSTOMER_TYPE':
        	if 1 == v:
        		li.append(2)
        	elif 2 == v:
        		li.append(1)
        	else:
        		li.append(None)
        elif k == 'LEGAL_REPRESENTATIVEID':
        	if 1 == li[1]:
        		li.append(15)
        		li.append(v)
        elif k == 'BUSINESSCODE':
        	if 2 == li[1]:
        		li.append(20)
        		li.append(v)
        else:
        	li.append(v)
    
    特殊操作：
    1. 解决Oracle乱码, 配置环境变量
        NLS_LANG    SIMPLIFIED CHINESE_CHINA.UTF8
        
    2. Oracle 连接环境变量配置
        Path        D:\ox_Oracle\instantclient_11_2
    

### 1. 项目结构
    db-dispose              # 项目工程
        - bin               # 可执行文件
        - core              # 核心代码
            -- exception    # 异常处理
            -- handler      # 逻辑处理
            -- params       # 对象属性
            -- test         # 项目测试
            -- ui           # 程序界面
            -- utils        # 工具包
        - docs              # 文档/文件
        - lib               # 静态库
        - logs              # 项目日志
        - resources         # 资源
            -- db           # 数据库配置
            -- logging.conf # 日志配置文件
            -- sqlite.db    # 批次号记录数据库
        app.py              # 主程序

### 2. Pyinstaller常用参数介绍:
    -i 图标路径
    -F 打包成一个exe文件
    -w 使用窗口，无控制台
    -c 使用控制台，无窗口
    -D 创建一个目录，里面包含exe以及其他一些依赖性文件
    --version-file 版本信息
    -n 指定程序名称
    pyinstaller -h 来查看参数
    
    Qt Designer[Qt 设计师]工具
        pip3 install pyqt5-tools

### 3. 打包可执行exe程序命令:
    pyinstaller -F -w app.py -i "D:\Workspaces\Python\db-dispose\resources\favicon.ico" --version-file=D:\Workspaces\Python\db-dispose\version.txt -n db-dispose.exe
    
### 联系我
    * QQ: 379839355
    * QQ群: [Æ┊Java✍交流┊Æ](https://jq.qq.com/?_wv=1027&k=5Dqlg2L)
    * QQ群: [Cute Python](https://jq.qq.com/?_wv=1027&k=58hW2jl)
    * Email: lmay@lmaye.com
    * Home: [lmaye.com](http://www.lmaye.com)
    * Blog: [blog.lmaye.com](http://www.lmaye.com)
    * GitHub: [lmayZhou](https://github.com/lmayZhou)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- 
# -- 开户记录更新
# import csv
#
# with open("D:\\sftp-test\\batch_open_12201803090000000001_20180723_28201807230000000008.csv", "r", encoding="UTF-8", newline="\n") as file:
#     readCSV = csv.reader(file, lineterminator="\n", delimiter=',')
#     content = [row for row in readCSV if row[len(row) - 1:][0] == '01']
#     for row in content:
#         print(row)
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Email lmay@lmaye.com
# Date: 2018/7/23 15:40 星期一
# ----------------------------------------------------------
from core.utils.excel_utils import read_csv_to_data
import datetime
from core.constant.sys_enum import SysEnum
from core.utils import jdbc_util
from core.utils.read_config import read_yml
from core.exception.custom_errors import HandleError


file_name = "batch_open_12201803090000000001_20180723_28201807230000000008.csv"
# 更新SQL
sql_1 = """
        update account_open_record set card_no = %s, customer_no = %s, serial_no = %s, ran_amount = %s, state = 3 
        where order_no = %s
    """

##############################################################
db_dispose_yml = read_yml(SysEnum.YML_PATH.value)
db_yml = read_yml(SysEnum.DB_PATH.value + SysEnum.SEPARATOR.value + db_dispose_yml["db-dispose"]["databases-yml"])
db_input_source = db_yml["db_input_source"]
try:
    int_connect = jdbc_util.sql_connect(db_input_source)
    print("{} [{}] 数据库-连接成功 ...".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                        db_input_source["database"]))
except HandleError as e:
    int_connect = None
    print(
        "{} [{}] 数据库-连接失败：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), db_input_source["database"],
                                     e.args[0]))
print("==============================================================================")

row = 0
data = read_csv_to_data(db_dispose_yml["db-dispose"]["excel-path"], file_name)
for item in data:
    if item[len(item) - 1:][0] == "01":
        args = [item[15 - 1], item[14 - 1], item[13 - 1], item[12 - 1], item[0]]
        row += jdbc_util.sql_execute(sql_1, "onlyOne", connect=int_connect, args=args)
        print("{} SQL执行条数：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), row))
        print("**************************************************************************")

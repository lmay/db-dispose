#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# --
# --
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Email lmay@lmaye.com
# Date: 2018/7/12 16:35 星期四
# ----------------------------------------------------------
import datetime
from dateutil.relativedelta import relativedelta
from core.utils.file_utils import write_file
from core.constant.sys_enum import SysEnum
from core.utils import jdbc_util
from core.utils.read_config import read_yml
from core.exception.custom_errors import HandleError

# 五月份统计
# may_sql = """
#             SELECT T2.PERIOD AS 融资期数,TO_CHAR(T2.UPDATEDATE, 'YYYYMMDD') AS 起息日期, T3.CHINESE_NAME AS 姓名, 0 AS 证件类型, CASE WHEN T3.CUSTOMER_TYPE = 1 THEN T3.BUSINESSCODE ELSE T3.PER_IDCARD END 证件号码,
#                    '91440300061443892W' AS 业务发生机构, T2.PRODUCT_CODE AS 业务号, CASE WHEN T2.PAYID = 1 THEN 3 ELSE 2 END 业务类型, 11 AS 业务种类, TO_CHAR(T3.OPEN_DATE, 'YYYYMMDD') AS 开户日期,
#                    TO_CHAR(T2.ENDDATE, 'YYYYMMDD') AS 到期日期, round(T2.REALITY_AMOUNT1) AS 授信额度, '{}' AS 业务发生日期, round(T4.amount) AS 余额, 0 AS 当前逾期总额, '{}' AS 本月还款状态
#             FROM PRODUCT T2
#             LEFT JOIN CMEMBER T3 ON T3.ID = T2.CMEMBERID
#             LEFT JOIN (SELECT a.PRODUCTID, SUM(a.amount) amount FROM INSTALMENTS a WHERE a.Pay_Time >= TO_DATE('20180601','YYYYMMDD') GROUP BY a.PRODUCTID) T4 ON T4.PRODUCTID = T2.ID
#             WHERE T2.PUBLICAUDITSTATE !=0 and T2.ENDDATE >= TO_DATE('20180501','YYYYMMDD') and T2.UPDATEDATE < TO_DATE('20180601','YYYYMMDD') AND T2.ptype_private = 1 AND T3.PER_IDCARD IS NOT NULL
#             AND T3.PER_IDCARD NOT IN('432930198003240268', '430624194911188721')
#             order by T3.PER_IDCARD
#         """

# 六月份统计
# june_sql = """
#             SELECT T2.PERIOD AS 融资期数,TO_CHAR(T2.UPDATEDATE, 'YYYYMMDD') AS 起息日期,T3.CHINESE_NAME AS 姓名, 0 AS 证件类型, CASE WHEN T3.CUSTOMER_TYPE = 1 THEN T3.BUSINESSCODE ELSE T3.PER_IDCARD END 证件号码,
#                    '91440300061443892W' AS 业务发生机构, T2.PRODUCT_CODE AS 业务号, CASE WHEN T2.PAYID = 1 THEN 3 ELSE 2 END 业务类型, 11 AS 业务种类, TO_CHAR(T3.OPEN_DATE, 'YYYYMMDD') AS 开户日期,
#                    TO_CHAR(T2.ENDDATE, 'YYYYMMDD') AS 到期日期, round(T2.REALITY_AMOUNT1) AS 授信额度, '{}' AS 业务发生日期, round(T4.amount) AS 余额, 0 AS 当前逾期总额, '{}' AS 本月还款状态
#             FROM PRODUCT T2
#             LEFT JOIN CMEMBER T3 ON T3.ID = T2.CMEMBERID
#             LEFT JOIN (SELECT a.PRODUCTID, SUM(a.amount) amount FROM INSTALMENTS a WHERE a.Pay_Time >= TO_DATE('20180701','YYYYMMDD') GROUP BY a.PRODUCTID) T4 ON T4.PRODUCTID = T2.ID
#             WHERE T2.PUBLICAUDITSTATE !=0 and T2.ENDDATE >= TO_DATE('20180601','YYYYMMDD') and T2.UPDATEDATE < TO_DATE('20180701','YYYYMMDD') AND T2.ptype_private = 1 AND T3.PER_IDCARD IS NOT NULL
#             AND T3.PER_IDCARD NOT IN('432930198003240268', '430624194911188721')
#             order by T3.PER_IDCARD
#             """

# 十月份统计
september_sql = """
            SELECT T2.PERIOD AS 融资期数,TO_CHAR(T2.UPDATEDATE, 'YYYYMMDD') AS 起息日期, T3.CHINESE_NAME AS 姓名, 0 AS 证件类型, CASE WHEN T3.CUSTOMER_TYPE = 1 THEN T3.BUSINESSCODE ELSE T3.PER_IDCARD END 证件号码, 
                   '91440300061443892W' AS 业务发生机构, T2.PRODUCT_CODE AS 业务号, CASE WHEN T2.PAYID = 1 THEN 3 ELSE 2 END 业务类型, 11 AS 业务种类, TO_CHAR(T3.OPEN_DATE, 'YYYYMMDD') AS 开户日期, 
                   TO_CHAR(T2.ENDDATE, 'YYYYMMDD') AS 到期日期, round(T2.REALITY_AMOUNT1) AS 授信额度, '{}' AS 业务发生日期, round(T4.amount) AS 余额, 0 AS 当前逾期总额, '{}' AS 本月还款状态
            FROM PRODUCT T2
            LEFT JOIN CMEMBER T3 ON T3.ID = T2.CMEMBERID
            LEFT JOIN (SELECT a.PRODUCTID, SUM(a.amount) amount FROM INSTALMENTS a WHERE a.Pay_Time >= TO_DATE('20181001','YYYYMMDD') GROUP BY a.PRODUCTID) T4 ON T4.PRODUCTID = T2.ID
            WHERE T2.PUBLICAUDITSTATE !=0 and T2.ENDDATE >= TO_DATE('20181001','YYYYMMDD') and T2.UPDATEDATE < TO_DATE('20181101','YYYYMMDD') AND T2.ptype_private = 1 AND T3.PER_IDCARD IS NOT NULL 
             AND T2.UPDATEDATE >= TO_DATE ( '20181001', 'YYYYMMDD' ) AND T2.UPDATEDATE < TO_DATE ( '20181101', 'YYYYMMDD' ) 
            order by T3.PER_IDCARD
        """

##############################################################
db_dispose_yml = read_yml(SysEnum.YML_PATH.value)
db_yml = read_yml(SysEnum.DB_PATH.value + SysEnum.SEPARATOR.value + db_dispose_yml["db-dispose"]["databases-yml"])
db_output_source = db_yml["db_output_source"]
try:
    out_connect = jdbc_util.sql_connect(db_output_source)
    print("{} [{}] 数据库-连接成功 ...".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                        db_output_source["database"]))
except HandleError as e:
    out_connect = None
    print("{} [{}] 数据库-连接失败：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                       db_output_source["database"], e.args[0]))

data_may = jdbc_util.sql_execute(september_sql, "select", connect=out_connect)
if data_may:
    _dt = ""
    for dt in data_may:
        # dt["姓名"] = dt["姓名"][0] + "某某"
        # new_date = datetime.datetime.strptime(dt["证件号码"][6:14], "%Y%m%d") + relativedelta(months=1, days=2)
        # dt["证件号码"] = dt["证件号码"][:6] + new_date.strftime("%Y%m%d") + dt["证件号码"][14:18]

        if dt["业务类型"] != 3:
            dt["到期日期"] = (datetime.datetime.strptime(dt["起息日期"], "%Y%m%d") + relativedelta(
                months=int(dt["融资期数"]))).strftime("%Y%m%d")

        if datetime.datetime.strptime(dt["到期日期"], "%Y%m%d") < datetime.datetime.strptime("20181101", "%Y%m%d"):
            dt["业务发生日期"] = dt["到期日期"]
            dt["本月还款状态"] = "G"
        elif datetime.datetime.strptime(dt["到期日期"], "%Y%m%d") > datetime.datetime.strptime("20181101", "%Y%m%d") \
                and dt["业务类型"] == 3:
            dt["业务发生日期"] = "20181031"
            dt["本月还款状态"] = "*"
        else:
            endYear = int(dt["到期日期"][0:4])
            endMonth = int(dt["到期日期"][4:6])
            diffMonth = (endYear - 2018) * 12 + (endMonth - 10)
            dt["业务发生日期"] = (datetime.datetime.strptime(dt["到期日期"], "%Y%m%d") - relativedelta(
                months=int(diffMonth))).strftime("%Y%m%d")
            dt["本月还款状态"] = "D"
        idx = 0
        for k, v in dt.items():
            idx += 1
            if idx <= 2:
                continue
            _dt += str(v if v else 0) + ("," if idx != len(dt) else "\r\n")

    write_file("121EXPORTTRADEINFO-1", ".txt", _dt, "D:\\")

# data_june = jdbc_util.sql_execute(june_sql, "select", connect=out_connect)
# if data_june:
#     _dt2 = ""
#     for dt in data_june:
#         dt["姓名"] = dt["姓名"][0] + "某某"
#         new_date = datetime.datetime.strptime(dt["证件号码"][6:14], "%Y%m%d") + relativedelta(months=1, days=2)
#         dt["证件号码"] = dt["证件号码"][:6] + new_date.strftime("%Y%m%d") + dt["证件号码"][14:18]
#
#         if dt["业务类型"] != 3:
#             dt["到期日期"] = (datetime.datetime.strptime(dt["起息日期"], "%Y%m%d") + relativedelta(
#                 months=int(dt["融资期数"]))).strftime("%Y%m%d")
#
#         if datetime.datetime.strptime(dt["到期日期"], "%Y%m%d") < datetime.datetime.strptime("20180701", "%Y%m%d"):
#             dt["业务发生日期"] = dt["到期日期"]
#             dt["本月还款状态"] = "G"
#         elif datetime.datetime.strptime(dt["到期日期"], "%Y%m%d") > datetime.datetime.strptime("20180701", "%Y%m%d") and dt[
#             "业务类型"] == 3:
#             dt["业务发生日期"] = "20180630"
#             dt["本月还款状态"] = "*"
#         else:
#             endYear = int(dt["到期日期"][0:4])
#             endMonth = int(dt["到期日期"][4:6])
#             diffMonth = (endYear - 2018) * 12 + (endMonth - 6)
#             dt["业务发生日期"] = (datetime.datetime.strptime(dt["到期日期"], "%Y%m%d") - relativedelta(
#                 months=int(diffMonth))).strftime("%Y%m%d")
#             dt["本月还款状态"] = "D"
#         idx = 0
#         for k, v in dt.items():
#             idx += 1
#             if idx <= 2:
#                 continue
#             _dt2 += str(v if v else 0) + ("," if idx != len(dt) else "\r\n")

    # write_file("121EXPORTTRADEINFO-2", ".txt", _dt2, "D:\\")

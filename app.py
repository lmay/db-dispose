#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's DB Dispose Main
# -- 主程序
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Date: 2018年2月2日 17:15:24
# ----------------------------------------------------------
import sys
from PyQt5.QtWidgets import QApplication
from core.ui.main_window import MainWindow

if __name__ == "__main__":
    app = QApplication(sys.argv)
    appMain = MainWindow()
    appMain.show()
    sys.exit(app.exec_())

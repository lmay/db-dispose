#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's Custom Exception
# -- 自定义异常
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Email lmay@lmaye.com
# Date: 2018/7/2 14:56 星期一
# ----------------------------------------------------------


class HandleError(Exception):
    """
        自定义
        - 处理异常
    """
    def __init__(self, responseEnum, *args):
        super().__init__(self)
        self.responseEnum = responseEnum
        self.args = args

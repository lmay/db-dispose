#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- 
# -- 
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Email lmay@lmaye.com
# Date: 2018/5/2 17:40 星期三
# ----------------------------------------------------------
from core.utils import jdbc_util
from core.constant.sys_enum import SysEnum
from core.utils.read_config import read_yml
from core.utils.excel_utils import write_data_to_csv

if __name__ == '__main__':
    # yml = read_yml(SysEnum.YML_PATH.value)
    # db_output_source = yml["db_input_source"]
    # out_connect = jdbc_util.sql_connect(db_output_source)
    # rs = jdbc_util.sql_execute("SELECT a.member_id, a.name, a.cert_type, a.cert_no, "
    #                            "mobile, a.bank_card_no, bank_mobile "
    #                            "FROM account_open_record a, account_bank_card_bind_record b "
    #                            "WHERE a.state = 3 and a.member_id = b.member_id", "select", connect=out_connect)
    # data = []
    # for row in rs:
    #     li = list(v for k, v in row.items())
    #     li.append(0)
    #     data.append(li)
    # write_data_to_csv("D://", "test", data)

    li = []
    # exec("if flag:\n\tli.append(1)\nelse:\n\tli.append(2)", {"flag": True, "li": li})
    # print(li)
    v = '1'
    #     if k == "SOURCES":
    #         if "1" == v:
    #             li.append(2)
    #         elif "2" == v:
    #             li.append(1)
    #         else:
    #             li.append(None)
    #     elif k == "SEX":
    #         if "1" == v:
    #             li.append("M")
    #         elif "2" == v:
    #             li.append("F")
    #         else:
    #             li.append("U")
    #     else:
    #         li.append(v)
    append_v_ = "if k=='SOURCES':\n\tif '1'==v:\n\t\tli.append(2)\n\telif '2'==v:\n\t\tli.append(1)\n\telse:\n\t\tli.append(None)\nelif k=='SEX':\n\tif '1'==v:\n\t\tli.append('M')\n\telif '2'==v:\n\t\tli.append('F')\n\telse:\n\t\tli.append('U')\nelse:\n\tli.append(v)"
    print(append_v_)
    exec(append_v_, {"k": "SEX", "li": li, "v": v})
    print(li)

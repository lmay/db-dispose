#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's Test
# -- 
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Date: 
# ----------------------------------------------------------
import json
import os

import yaml

current_path = os.path.abspath("../../resources/db")


def read_yml(path):
    with open(path + os.path.sep + 'databases.yml', 'r') as f:
        return yaml.load(f.read())


def read_json(path):
    with open(path + os.path.sep + 'sql.json', 'r') as js:
        return json.loads(js.read())


if __name__ == "__main__":
    print(read_yml(current_path))
    print(read_json(current_path))
    print("resources_path".upper())

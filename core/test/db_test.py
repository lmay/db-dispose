#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- 
# -- 
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Email lmay@lmaye.com
# Date: 2018/5/7 16:17 星期一
# ----------------------------------------------------------
import datetime
from core.constant.sys_enum import SysEnum
from core.utils import jdbc_util

if __name__ == '__main__':
    db_source = {"database": SysEnum.SQLITE_DB.value, "db_type": "sqlite"}
    connect = jdbc_util.sql_connect(db_source)
    create_tab = """
                create table if not exists generate_id(
                  id integer primary key autoincrement unique not null,
                  uid varchar(10),
                  ext varchar(255),
                  last_date timestamp not null,
                  version integer(10) not null
                );
                """
    jdbc_util.sql_execute(create_tab, "create", connect)
    insert_sql = "insert into generate_id(uid, last_date, version) values (?, ?, ?)"
    # jdbc_util.sql_execute(insert_sql, "insert", connect, args=("1000000", "1000000", "1000000", "1000000",
    #                                                            datetime.datetime.now(), 1))
    result = jdbc_util.sql_execute("select uid, version from generate_id", "select", connect)
    print(result)
    result = jdbc_util.sql_execute("select uid, version from generate_id where id = 1", "select", connect)
    uid = result[0]["uid"]
    version = result[0]["version"]
    up_sql = "update generate_id set uid = ?, last_date = ?, version = ? where id = 1"
    jdbc_util.sql_execute(up_sql, "update", connect, (str(int(uid) + 1), datetime.datetime.now(), version + 1))

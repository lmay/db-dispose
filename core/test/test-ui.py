# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'test.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!
import sys

from PyQt5 import QtCore
from PyQt5.QtWidgets import QMainWindow, QWidget, QCalendarWidget, QApplication


class Ui_MainWindow(QMainWindow):
    def setupUi(self):
        self.setObjectName("MainWindow")
        self.resize(800, 600)
        self.centralwidget = QWidget()
        self.centralwidget.setObjectName("centralwidget")
        self.calendarWidget = QCalendarWidget(self.centralwidget)
        self.calendarWidget.setGeometry(QtCore.QRect(70, 40, 248, 197))
        self.calendarWidget.setObjectName("calendarWidget")
        self.setCentralWidget(self.centralwidget)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ui = Ui_MainWindow()
    ui.setupUi()
    ui.show()
    sys.exit(app.exec_())

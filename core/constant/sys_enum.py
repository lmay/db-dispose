#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's System Enum
# -- 常量枚举值
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Date: 2018年2月13日 15:44:23
# ----------------------------------------------------------
import os
from enum import Enum, unique


@unique
class SysEnum(Enum):
    """
        System Enum
    """
    SEPARATOR = os.path.sep
    CURRENT_PATH = os.path.abspath(".")
    RESOURCES_PATH = CURRENT_PATH + SEPARATOR + "resources"
    FAVICON_PATH = RESOURCES_PATH + SEPARATOR + "favicon.png"
    YML_PATH = RESOURCES_PATH + SEPARATOR + "db-dispose.yml"
    LOGGING_PATH = RESOURCES_PATH + SEPARATOR + "logging.conf"
    DB_PATH = RESOURCES_PATH + SEPARATOR + "db"
    SQLITE_DB = RESOURCES_PATH + SEPARATOR + "sqlite.db"

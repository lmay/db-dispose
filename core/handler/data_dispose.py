#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's Databases Dispose
# -- 数据处理
# ****************************
# Author: lmay.Zhou
# Email: lmay@lmaye.com
# Blog: www.lmaye.com
# Date: 2018/4/27 0:14 星期五
# ----------------------------------------------------------
import datetime
from core.utils import jdbc_util
from core.utils.db_data_dispose import data_dispose
from core.exception.custom_errors import HandleError
from core.logger.log import logger
LOGGER = logger()


def db_normal_dispose(tb_key, out_connect, int_connect, output_db, input_db, signal_out=None):
    """
        This's databases normal dispose.

        :param tb_key:      处理SQL键
        :param out_connect: 导入连接
        :param int_connect: 导入连接
        :param output_db:   导出数据库
        :param input_db:    导入数据库
        :param signal_out:  信号槽
        :return:
    """
    try:
        # 查出总条数
        count_sql = "select count(*) TOTAL from ({}) t".format(output_db[tb_key]["normal_sql"] + (" where " + output_db[tb_key]["where"] if output_db[tb_key]["where"] else ""))
        rs = jdbc_util.sql_execute(count_sql, "select", connect=out_connect)
        if not rs:
            return None
        row_total = rs[0]["TOTAL"]
        signal_out.emit("{} 待执行[{}]总条数：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                  output_db[tb_key]["tb_name"], row_total))
        output_sql = output_db[tb_key]["normal_sql"]
        # 查询条件
        output_sql += " where {}".format(output_db[tb_key]["where"]) if output_db[tb_key]["where"] else ""

        # 执行SQL
        data = jdbc_util.sql_execute(output_sql, "select", connect=out_connect)
        if data:
            signal_out.emit("{} [{}] 待处理条数：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                      output_db[tb_key]["tb_name"], len(data)))
            LOGGER.debug("{} [{}] 待处理条数：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                   output_db[tb_key]["tb_name"], len(data)))
            input_sql = input_db[tb_key]["normal_sql"]
            row = jdbc_util.sql_execute(input_sql, "insert", connect=int_connect,
                                        args=data_dispose(data, output_db[tb_key]["special_dispose"], signal_out))
            LOGGER.debug("执行[{}]处理条数：{}".format(input_db[tb_key]["tb_name"], row))
            signal_out.emit("{} 执行[{}]处理条数：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                      input_db[tb_key]["tb_name"], row))
            signal_out.emit("**************************************************************************")
        else:
            return None
    except HandleError as e:
        if signal_out:
            signal_out.emit("{} {}{}：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), e.args[0],
                                                e.args[1], e.args[2]))
        return None


def db_page_dispose(db_type, tb_key, out_connect, rows, int_connect, output_db, input_db, signal_out=None):
    """
        This's databases page dispose.

        :param db_type:         数据库类型
        :param tb_key:          处理SQL键
        :param out_connect:     导出连接
        :param rows:            分页处理行数
        :param int_connect:     导入连接
        :param output_db:       导出数据库
        :param input_db:        导入数据库
        :param signal_out:      信号槽
        :return:
    """
    try:
        # 查出总条数
        count_sql = "select count(*) TOTAL from ({}) t".format(output_db[tb_key]["normal_sql"])
        # 查询条件
        count_sql = count_sql + " where " + output_db[tb_key]["where"] if output_db[tb_key]["where"] else count_sql
        rs = jdbc_util.sql_execute(count_sql, "select", connect=out_connect)
        if not rs:
            return None
        row_total = rs[0]["TOTAL"]
        signal_out.emit("{} 待执行[{}]总条数：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                  output_db[tb_key]["tb_name"], row_total))
        # 分页参数
        page_curr = 1
        page_total = int(row_total / rows) if row_total % rows == 0 else int(row_total / rows) + 1
        while page_total > 0:
            end_site = rows
            start_site = page_curr
            if db_type == "oracle":
                # oracle 分页
                end_site = rows * page_curr
                start_site = rows * (page_curr - 1)
                output_sql = output_db[tb_key]["page_sql"].format(end_site, start_site)
                # 查询条件
                output_sql += " and {}".format(output_db[tb_key]["where"]) if output_db[tb_key]["where"] else ""
            elif db_type == "mysql":
                # mysql 分页
                start_site = rows * (page_curr - 1)
                output_sql = output_db[tb_key]["page_sql"].format("{}", start_site, rows)
                output_sql = output_sql.format("where " + output_db[tb_key]["where"]) \
                    if output_db[tb_key]["where"] else output_sql.format("")
            else:
                signal_out.emit("Sorry, 暂不支持[{}]数据库...".format(db_type))
            # 执行SQL
            data = jdbc_util.sql_execute(output_sql, "select", connect=out_connect)
            if signal_out:
                signal_out.emit("{} 执行SQL: {}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), output_sql))
            if data:
                signal_out.emit("{} [{}] 分页查询 - [{}] <<>> [{}] 待处理条数：{}".format(
                    datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), output_db[tb_key]["tb_name"], start_site,
                    end_site, len(data)))
                LOGGER.debug("{} [{}] 分页查询 - [{}] <<>> [{}] 待处理条数：{}".format(
                    datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), output_db[tb_key]["tb_name"],
                    start_site, end_site, len(data)))
                input_sql = input_db[tb_key]["normal_sql"]
                row = jdbc_util.sql_execute(input_sql, "insert", connect=int_connect,
                                            args=data_dispose(data, output_db[tb_key]["special_dispose"], signal_out))
                if signal_out:
                    signal_out.emit("{} 执行SQL: {}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), input_sql))
                LOGGER.debug("执行[{}]处理条数：{}".format(input_db[tb_key]["tb_name"], row))
                signal_out.emit("{} 执行[{}]处理条数：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                          input_db[tb_key]["tb_name"], row))
                signal_out.emit("**************************************************************************")
                page_curr += 1
                page_total -= 1
            else:
                return None
    except HandleError as e:
        if signal_out:
            signal_out.emit("{} {}{}：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), e.args[0],
                                                e.args[1], e.args[2]))
        return None

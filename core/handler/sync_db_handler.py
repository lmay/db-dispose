#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's Databases Sync Handler
# -- 数据处理线程
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Date: 2018年2月1日 18:08:32
# ----------------------------------------------------------
import datetime
from PyQt5.QtCore import pyqtSignal, QThread
from core.constant.sys_enum import SysEnum
from core.utils import jdbc_util
from core.utils.read_config import read_yml, read_json
from core.handler.data_dispose import db_page_dispose, db_normal_dispose
from core.exception.custom_errors import HandleError
from core.logger.log import logger
LOGGER = logger()


class SyncExecuteThread(QThread):
    """
        SyncExecuteThread

        -- 同步执行线程
    """
    signal_out = pyqtSignal(str)

    def __init__(self, parent=None):
        super(SyncExecuteThread, self).__init__(parent)
        self.threadName = "MyThread"
        self.rows = None

    def set_rows(self, rows):
        self.rows = rows

    def run(self):
        """
            Synchronization Data
        """
        self.signal_out.emit("{} -->>> 开始执行 ...".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
        try:
            db_dispose_yml = read_yml(SysEnum.YML_PATH.value)
            db_yml = read_yml(SysEnum.DB_PATH.value + SysEnum.SEPARATOR.value + db_dispose_yml["db-dispose"]["databases-yml"])
            db_output_source = db_yml["db_output_source"]
            db_input_source = db_yml["db_input_source"]
            json = read_json(SysEnum.DB_PATH.value + SysEnum.SEPARATOR.value + db_yml["sql_json"])
            output_db = json["output_db"]
            input_db = json["input_db"]
        except Exception as e:
            self.signal_out.emit("{} 解析异常: {}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), e))
            return None

        try:
            out_connect = jdbc_util.sql_connect(db_output_source)
            self.signal_out.emit("{} [{}] 数据库-连接成功 ...".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                              db_output_source["database"]))
        except HandleError as e:
            out_connect = None
            self.signal_out.emit("{} [{}] 数据库-连接失败：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                             db_output_source["database"], e.args[0]))

        try:
            int_connect = jdbc_util.sql_connect(db_input_source)
            self.signal_out.emit("{} [{}] 数据库-连接成功 ...".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                              db_input_source["database"]))
        except HandleError as e:
            int_connect = None
            self.signal_out.emit("{} [{}] 数据库-连接失败：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                             db_input_source["database"], e.args[0]))
        self.signal_out.emit("==============================================================================")
        if out_connect and int_connect:
            # 遍历处理SQL
            for tb_key in output_db:
                if self.rows and self.rows > 0:
                    db_page_dispose(db_output_source["db_type"], tb_key, out_connect, self.rows, int_connect, output_db,
                                    input_db, signal_out=self.signal_out)
                else:
                    db_normal_dispose(tb_key, out_connect, int_connect, output_db, input_db, signal_out=self.signal_out)
        # 关闭连接
        if out_connect:
            jdbc_util.sql_close(db_output_source["db_type"], out_connect)
            self.signal_out.emit("{} [数据库连接关闭] {} connect to close.".format(datetime.datetime.now()
                                                                            .strftime('%Y-%m-%d %H:%M:%S'),
                                                                            db_output_source["db_type"]))

        if int_connect:
            jdbc_util.sql_close(db_input_source["db_type"], int_connect)
            self.signal_out.emit("{} [数据库连接关闭] {} connect to close.".format(datetime.datetime.now()
                                                                            .strftime('%Y-%m-%d %H:%M:%S'),
                                                                            db_input_source["db_type"]))
        self.signal_out.emit("==============================================================================")

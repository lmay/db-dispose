#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's Excel Dispose
# -- 数据处理
# ****************************
# Author: lmay.Zhou
# Email: lmay@lmaye.com
# Blog: www.lmaye.com
# Date: 2018/4/27 0:14 星期五
# ----------------------------------------------------------
import datetime
from core.constant.sys_enum import SysEnum
from core.utils import jdbc_util
from core.utils.excel_utils import write_data_to_csv
from core.utils.db_data_dispose import data_dispose
from core.exception.custom_errors import HandleError
from core.logger.log import logger
LOGGER = logger()


def csv_dispose(db_type, tb_key, out_connect, rows, output_db, db_dispose_yml, signal_out=None):
    """
        This's databases page dispose.

        :param db_type:         数据库类型
        :param tb_key:          处理SQL键
        :param out_connect:     导出连接
        :param rows:            分页处理行数
        :param output_db:       导出数据库
        :param db_dispose_yml:  YML配置
        :param signal_out:      信号槽
        :return:
    """
    try:
        db_source = {"database": SysEnum.SQLITE_DB.value, "db_type": "sqlite"}
        connect = jdbc_util.sql_connect(db_source)
        # 查出总条数
        count_sql = "select count(*) TOTAL from ({}) t".format(output_db[tb_key]["normal_sql"] + (" where " + output_db[tb_key]["where"] if output_db[tb_key]["where"] else ""))
        rs = jdbc_util.sql_execute(count_sql, "select", connect=out_connect)
        if not rs:
            return None
        row_total = rs[0]["TOTAL"]
        signal_out.emit("{} 待执行[{}]总条数：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                  output_db[tb_key]["tb_name"], row_total))
        # 分页参数
        page_curr = 1
        page_total = int(row_total / rows) if row_total % rows == 0 else int(row_total / rows) + 1
        while page_total > 0:
            end_site = rows
            start_site = page_curr
            if db_type == "oracle":
                # oracle 分页
                end_site = rows * page_curr
                start_site = rows * (page_curr - 1)
                output_sql = output_db[tb_key]["page_sql"].format(end_site, start_site)
                # 查询条件
                output_sql += " and {}".format(output_db[tb_key]["where"]) if output_db[tb_key]["where"] else ""
            elif db_type == "mysql":
                # mysql 分页
                start_site = rows * (page_curr - 1)
                output_sql = output_db[tb_key]["page_sql"].format("{}", start_site, rows)
                output_sql = output_sql.format("where " + output_db[tb_key]["where"]) \
                    if output_db[tb_key]["where"] else output_sql.format("")
            else:
                signal_out.emit("Sorry, 暂不支持[{}]数据库...".format(db_type))
            # 执行SQL
            data = jdbc_util.sql_execute(output_sql, "select", connect=out_connect)
            if data:
                signal_out.emit("{} [{}] 分页查询 - [{}] <<>> [{}] 待处理条数：{}".format(
                    datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), output_db[tb_key]["tb_name"], start_site,
                    end_site, len(data)))
                LOGGER.debug("{} [{}] 分页查询 - [{}] <<>> [{}] 待处理条数：{}".format(
                    datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), output_db[tb_key]["tb_name"],
                    start_site, end_site, len(data)))
                result = jdbc_util.sql_execute("select uid, version from generate_id where id = 1", "select", connect)
                if not result:
                    return None
                uid = result[0]["uid"]
                # 生成Excel
                content = data_dispose(data, output_db[tb_key]["special_dispose"], signal_out)
                version = result[0]["version"]
                file_name = db_dispose_yml["db-dispose"]["excel-name"] + uid + ".csv"
                write_data_to_csv(db_dispose_yml["db-dispose"]["excel-path"], file_name, content)
                signal_out.emit("{} [{}]写入Excel条数：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                             file_name, len(data)))
                up_sql = "update generate_id set uid = ?, last_date = ?, version = ? where id = 1"
                jdbc_util.sql_execute(up_sql, "onlyOne", connect, (str(int(uid) + 1), datetime.datetime.now(), version + 1))
                signal_out.emit("**************************************************************************")
                page_curr += 1
                page_total -= 1
        jdbc_util.sql_close("sqlite", connect)
    except HandleError as e:
        if signal_out:
            signal_out.emit("{} {}{}：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), e.args[0],
                                                e.args[1], e.args[2]))
        return None

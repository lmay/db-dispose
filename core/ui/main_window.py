#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's Client Main Frame Window
# -- 客户端主界面
# ****************************
# Author: lmaye.Zhou
# Blog: www.lmay.com
# Date: 2018年2月2日 15:57:01
# ----------------------------------------------------------
import datetime
from PyQt5.QtGui import QIcon, QIntValidator
from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout, QGroupBox, QFormLayout, QWidget, QGridLayout, QPushButton, \
    QListWidget, QLineEdit, QLabel
from core.constant.sys_enum import SysEnum
from core.handler.sync_db_handler import SyncExecuteThread
from core.handler.sync_excel_handler import SyncGenerateExcelThread
from core.logger.log import logger
LOGGER = logger()


class MainWindow(QWidget):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.initQMainWindow()

    def initQMainWindow(self):
        """
            初始化界面

            :return:
        """
        self.creatFormGroupBox()
        self.createDbGridGroupBox()
        self.createExcelGridGroupBox()
        main_layout = QVBoxLayout()
        hbox_layout = QHBoxLayout()
        hbox_layout.addWidget(self.grid_group_db)
        hbox_layout.addWidget(self.grid_group_excel)
        main_layout.addWidget(self.form_group_box)
        main_layout.addLayout(hbox_layout)
        self.setLayout(main_layout)
        self.setFixedSize(900, 600)
        self.setWindowTitle("数据库数据同步 - 客户端")
        self.setWindowIcon(QIcon(SysEnum.FAVICON_PATH.value))

    def creatFormGroupBox(self):
        """
            UI 日志输出区域

            :return:
        """
        self.form_group_box = QGroupBox("输出区域")
        layout = QFormLayout()
        self.text_edit = QListWidget()
        self.text_edit.setFixedSize(860, 480)
        layout.addRow(self.text_edit)
        self.form_group_box.setLayout(layout)

    def createDbGridGroupBox(self):
        """
            Databases 操作区域

            :return:
        """
        self.grid_group_db = QGroupBox("Databases 操作区域")
        layout = QGridLayout()
        btn_normal = QPushButton("全部执行")
        btn_normal.clicked.connect(self.execute_all)

        btn_page = QPushButton("分页执行")
        btn_page.clicked.connect(self.execute_page)

        label = QLabel("执行行数:")
        self.execute_total = QLineEdit()
        # 只允许输入int类型
        self.execute_total.setValidator(QIntValidator())
        self.execute_total.setText("10000")
        self.execute_total.setPlaceholderText("请输入执行行数")

        layout.addWidget(btn_normal, 1, 0)
        layout.addWidget(btn_page, 1, 1)
        layout.addWidget(label, 1, 2)
        layout.addWidget(self.execute_total, 1, 3)
        layout.setColumnStretch(5, 1)
        self.grid_group_db.setLayout(layout)

    def createExcelGridGroupBox(self):
        """
            Excel 操作区域

            :return:
        """
        self.grid_group_excel = QGroupBox("Excel 操作区域")
        layout = QGridLayout()

        label = QLabel("生成行数:")
        self.excel_total = QLineEdit()
        # 只允许输入int类型
        self.excel_total.setValidator(QIntValidator())
        self.excel_total.setText("5000")
        self.excel_total.setPlaceholderText("请输入生成行数")

        btn_excel = QPushButton("生成excel")
        btn_excel.clicked.connect(self.execute_excel)
        layout.addWidget(label, 1, 0)
        layout.addWidget(self.excel_total, 1, 1)
        layout.addWidget(btn_excel, 1, 2)
        layout.setColumnStretch(5, 1)
        self.grid_group_excel.setLayout(layout)

    def printMsg(self, log):
        """
            界面打印日志

            :param log: 日志
            :return:
        """
        self.text_edit.addItem(log)
        # 自动滚动
        self.text_edit.setCurrentRow(self.text_edit.count() - 1)

    def execute_page(self):
        """
            分页处理信号槽

            :return:
        """
        rows = self.execute_total.text().strip()
        if not rows:
            self.printMsg("{} 分页执行模式: 执行行数必填".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
            return
        if int(rows) <= 0:
            self.printMsg("{} 分页执行模式: 执行行数必需大于等于1".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
            return
        self.t1 = SyncExecuteThread()
        self.t1.set_rows(int(rows))
        self.t1.signal_out.connect(self.printMsg)
        self.t1.start()

    def execute_all(self):
        """
            常规处理信号槽

            :return:
        """
        self.t2 = SyncExecuteThread()
        self.t2.signal_out.connect(self.printMsg)
        self.t2.start()

    def execute_excel(self):
        """
            生成excel处理处理信号槽

            :return:
        """
        rows = self.excel_total.text().strip()
        if not rows:
            self.printMsg("{} 生成excel模式: 生成行数必填".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
            return
        if int(rows) <= 0:
            self.printMsg("{} 生成excel模式: 生成行数必需大于等于1".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
            return
        self.t3 = SyncGenerateExcelThread()
        self.t3.set_rows(int(rows))
        self.t3.signal_out.connect(self.printMsg)
        self.t3.start()

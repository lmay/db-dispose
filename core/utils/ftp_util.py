#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's FTP Utils
# -- 
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Email lmay@lmaye.com
# Date: 2018/5/2 14:55 星期三
# ----------------------------------------------------------
from ftplib import FTP


def ftp_connect(host, username, password):
    ftp = FTP()
    # ftp.set_debuglevel(2)
    ftp.connect(host, 21)
    ftp.login(username, password)
    return ftp


# 从ftp下载文件
def download_file(ftp, remote_path, local_path):
    buf_size = 1024
    fp = open(local_path, 'wb')
    ftp.retrbinary('RETR ' + remote_path, fp.write, buf_size)
    ftp.set_debuglevel(0)
    fp.close()


# 从本地上传文件到ftp
def upload_file(ftp, remote_path, local_path):
    buf_size = 1024
    fp = open(local_path, 'rb')
    ftp.storbinary('STOR ' + remote_path, fp, buf_size)
    ftp.set_debuglevel(0)
    fp.close()


if __name__ == "__main__":
    ftp = ftp_connect("113.105.139.xxx", "ftp***", "Guest***")
    download_file(ftp, "Faint.mp4", "C:/Users/Administrator/Desktop/test.mp4")
    # 调用本地播放器播放下载的视频
    # os.system('start "C:\Program Files\Windows Media Player\wmplayer.exe" "C:/Users/Administrator/Desktop/test.mp4"')
    upload_file(ftp, "C:/Users/Administrator/Desktop/test.mp4", "test.mp4")
    ftp.quit()

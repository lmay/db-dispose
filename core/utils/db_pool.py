#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's DB Pool
# -- 数据库连接池
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Email lmay@lmaye.com
# Date: 2018/7/9 10:46 星期一
# ----------------------------------------------------------
import importlib
from DBUtils.PooledDB import PooledDB
from MySQLdb.cursors import DictCursor
from core.exception.custom_errors import HandleError
from core.constant.response_enum import ResponseEnum
from core.logger.log import logger
LOGGER = logger()


class DatabasesPool(object):
    """
        数据库对象，负责产生数据库连接 , 此类中的连接采用连接池实现
    """
    # 连接池对象
    __pool = None

    def __init__(self, db_source):
        """
            数据库构造函数，从连接池中取出连接，并生成操作游标
        """
        self._conn = self.__get_connect(db_source)
        self._cursor = self._conn.cursor()

    @staticmethod
    def __get_connect(db_source):
        """
            PooledDB使用方法同PersistentDB，只是参数有所不同。

            dbapi：数据库接口\n
            mincached：启动时开启的空连接数量\n
            maxcached：连接池最大可用连接数量\n
            maxshared：连接池最大可共享连接数量\n
            maxconnections：最大允许连接数量\n
            blocking：达到最大数量时是否阻塞\n
            maxusage：单个连接最大复用次数\n
            setsession：用于传递到数据库的准备会话，如 [”set name UTF-8″]

            :param db_source: 数据源
            :return:
        """
        if not DatabasesPool.__pool:
            __db_type = db_source["db_type"]
            if "mysql" == __db_type:
                __pool = PooledDB(creator=importlib.import_module("MySQLdb"), mincached=db_source["mincached"],
                                  maxcached=db_source["maxcached"], host=db_source["host"], port=db_source["port"],
                                  user=db_source["user"], passwd=db_source["password"], db=db_source["database"],
                                  charset=db_source["charset"], cursorclass=DictCursor)
            else:
                LOGGER.error("数据库: {}".format(ResponseEnum.TYPE_ERROR.value["msg"]))
                raise HandleError(ResponseEnum.TYPE_ERROR.value, "数据库-配置有误[{}]".format(
                    ResponseEnum.TYPE_ERROR.value["msg"]))
        return __pool.connection()

    def sql_execute(self, sql, handle=None, param=None):
        """
            Mysql Execute

            :param str sql: Execute sql statement.
            :param handle: It's insert, delete, update or select.
            :type handle: str or None
            :param list param: Sequence of sequences or mappings.  It is used as parameter.
            :return: Execute result
            :rtype: object
        """
        try:
            # self.begin()
            LOGGER.info("Execute sql: {}".format(sql))
            if "select" == handle:
                self._cursor.execute(sql)
                result = self._cursor.fetchall()
            else:
                if param:
                    result = self._cursor.executemany(sql, param)
                else:
                    result = self._cursor.execute(sql)
                self.end("commit")
            LOGGER.info("Execute success: {}".format(result))
            self.dispose()
            return result
        except Exception as e:
            LOGGER.error("操作失败{}：{}".format(e.__class__, e))
            if "select" != handle:
                self.end(False)
            raise HandleError(ResponseEnum.FAILURE.value, "数据库-操作失败", e.__class__, e)

    def begin(self):
        """
            开启事务

            :return:
        """
        self._conn.autocommit(0)

    def end(self, option=None):
        """
            结束事务

            :param option:
            :return:
        """
        if option == "commit":
            self._conn.commit()
        else:
            self._conn.rollback()

    def dispose(self):
        """
            释放连接池资源

            :return:
        """
        self._cursor.close()
        self._conn.close()

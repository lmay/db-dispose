#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's Read Config File
# -- 
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Date: 2018年4月26日 16:56:56
# ----------------------------------------------------------
import json
import yaml
from core.logger.log import logger
LOGGER = logger()


def read_json(path=None):
    """
        This's read JSON config file.

        :param path: JSON config file path.
        :type path: str or None
        :return: JSON object
        :rtype: object
    """
    if path:
        try:
            with open(path, "r", encoding="UTF-8") as js:
                return json.loads(js.read())
        except Exception as e:
            LOGGER.error("JSON parse exception: {}".format(e))
            raise Exception("JSON parse exception !")
    else:
        LOGGER.error("JSON config file path is None ...")
        return None


def read_yml(path=None):
    """
        This's read yml config file.

        :param path: Yml config file path.
        :type path: str or None
        :return: Yml object
        :rtype: object
    """
    if path:
        try:
            with open(path, "r", encoding="UTF-8") as yml:
                return yaml.load(yml.read())
        except Exception as e:
            LOGGER.error("Yml parse exception: {}".format(e))
            raise Exception("Yml parse exception !")
    else:
        LOGGER.error("Yml config file path is None ...")
        return None

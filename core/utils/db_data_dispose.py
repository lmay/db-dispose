#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's DB Data Dispose
# -- 数据库特殊字段数据处理
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Email lmay@lmaye.com
# Date: 2018/5/10 20:50 星期四
# ----------------------------------------------------------
from core.logger.log import logger
LOGGER = logger()


def data_dispose(data, special_dispose=None, signal_out=None, **kwargs):
    """
        数据处理

        :param data: 数据
        :param special_dispose: 处理字段
        :param signal_out: 组件
        :param kwargs: 其他参数
        :return: 处理后的数据
    """
    if special_dispose:
        LOGGER.info("数据处理 - 表达式: {}".format(special_dispose))
        # if signal_out:
        #     signal_out.emit("数据处理 - 表达式: {}".format(special_dispose))
    result = []
    for row in data:
        li = []
        _dict = {}
        for k, v in row.items():
            # 批次号
            if kwargs and len(li) == 0:
                li.append(kwargs["batch_no"])
            if special_dispose:
                try:
                    exec(special_dispose, {"k": k, "li": li, "v": v, "_dict": _dict})
                except Exception as e:
                    LOGGER.error("数据处理 - 表达式执行异常: {}\n{}".format(e, special_dispose))
                    if signal_out:
                        signal_out.emit("数据处理 - 表达式执行异常: {}\n{}".format(e, special_dispose))
                # if k == 'CUSTOMER_TYPE':
                #     if 1 == v:
                #         li.append(2)
                #     elif 2 == v:
                #         li.append(1)
                #     else:
                #         li.append(None)
                # elif k == 'LEGAL_REPRESENTATIVEID':
                #     if 1 == li[1]:
                #         li.append(15)
                #         li.append(v)
                # elif k == 'BUSINESSCODE':
                #     if 2 == li[1]:
                #         li.append(20)
                #         li.append(v)
                # else:
                #     li.append(v)
                # ------------------- 数据字典
                # if k == 'ORGANISATIONCODE':
                #     if v:
                #         _dict[k] = v
                #     else:
                #         _dict[k] = ''
                # elif k == 'IRSNUMBER':
                #     if v:
                #         _dict[k] = v
                #     else:
                #         _dict[k] = ''
                #     li.append(str(_dict))
                # else:
                #     li.append(v)
            else:
                li.append(v)
        result.append(li)
    return result

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- This's File Utils
# -- 文件处理工具集
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# @email lmay@lmaye.com
# Date: 2018年4月11日 00:31:23
# ----------------------------------------------------------
import os
from core.constant.sys_enum import SysEnum
from core.logger.log import logger
LOGGER = logger()


def write_file(file_name, file_suffix, content, output_path):
    """
        文件生成

        :param str file_name:   文件名
        :param str file_suffix: 文件后缀
        :param str content:     写入内容
        :param str output_path: 输出路径
        :return:                None
    """
    if not file_name:
        LOGGER.error("File name is None.")
        return None
    if not file_suffix:
        LOGGER.error("File suffix is None.")
        return None
    if not output_path:
        LOGGER.error("Output path is None.")
        return None
    # 目录是否存在
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    """
        1、\n 软回车：
        在Windows 中表示换行且回到下一行的最开始位置。相当于Mac OS 里的 \r 的效果。
        在Linux、unix 中只表示换行，但不会回到下一行的开始位置。
        
        2、\r 软空格：
        在Linux、unix 中表示返回到当行的最开始位置。
        在Mac OS 中表示换行且返回到下一行的最开始位置，相当于Windows 里的 \n 的效果。
        
        'U' mode is deprecated and will raise an exception in future versions of Python.
        It has no effect in Python 3. Use newline to control universal newlines mode.
        ※ Python3 newline="\n" [指定换行符]
    """
    f = open(output_path + SysEnum.SEPARATOR.value + file_name + file_suffix, 'w+', encoding='GBK', newline="\n")
    LOGGER.info("写入内容：{}".format(content))
    f.write(content)
    f.close()
    LOGGER.info("文件写入成功: {}".format(output_path + SysEnum.SEPARATOR.value + file_name + file_suffix))

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- 
# -- 
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Email lmay@lmaye.com
# Date: 2018/7/2 14:00 星期一
# ----------------------------------------------------------
import datetime
from core.constant.sys_enum import SysEnum
from core.utils import jdbc_util
from core.utils.read_config import read_yml
from core.exception.custom_errors import HandleError


# 查出总条数
count_sql = "select count(*) TOTAL from (select bid_order_no from bid_repayment_plan where period > 1 group by bid_order_no) t"

# 分页查bid_order_no
page_sql = "select bid_order_no from bid_repayment_plan where period > 1 group by bid_order_no limit {}, {}"

# 查询id, amount, interest
sql_t1 = "select id, amount, interest from INSTALMENTS_DMEMBER where subid = {} order by num DESC"

# 插入临时表
temp_sql = "INSERT INTO bid_repayment_plan_temp (id, next_plan_id, surplus_principal, surplus_interest) VALUES (%s, %s, %s, %s)"

##############################################################
db_dispose_yml = read_yml(SysEnum.YML_PATH.value)
db_yml = read_yml(SysEnum.DB_PATH.value + SysEnum.SEPARATOR.value + db_dispose_yml["db-dispose"]["databases-yml"])
db_output_source = db_yml["db_output_source"]
db_input_source = db_yml["db_input_source"]
try:
    out_connect = jdbc_util.sql_connect(db_output_source)
    print("{} [{}] 数据库-连接成功 ...".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                        db_output_source["database"]))
except HandleError as e:
    out_connect = None
    print("{} [{}] 数据库-连接失败：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                       db_output_source["database"], e.args[0]))

try:
    int_connect = jdbc_util.sql_connect(db_input_source)
    print("{} [{}] 数据库-连接成功 ...".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                        db_input_source["database"]))
except HandleError as e:
    int_connect = None
    print(
        "{} [{}] 数据库-连接失败：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), db_input_source["database"],
                                     e.args[0]))
print("==============================================================================")

# 查询条件
rs = jdbc_util.sql_execute(count_sql, "select", connect=int_connect)
if rs:
    row_total = rs[0]["TOTAL"]
    print("{} 待执行总条数：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), row_total))
    # 分页参数
    rows = 1000
    page_curr = 1
    page_total = int(row_total / rows) if row_total % rows == 0 else int(row_total / rows) + 1
    while page_total > 0:
        end_site = rows
        start_site = page_curr
        # mysql 分页
        start_site = rows * (page_curr - 1)
        limit_sql = page_sql.format(start_site, rows)
        # 执行SQL
        data = jdbc_util.sql_execute(limit_sql, "select", connect=int_connect)
        if data:
            print("{} 分页查询 - [{}] <<>> [{}] 待处理条数：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), start_site, end_site, len(data)))
            args = []
            for it in data:
                sql_2 = sql_t1.format(it["bid_order_no"]).replace("[", "").replace("]", "")
                data_2 = jdbc_util.sql_execute(sql_2, "select", connect=out_connect)
                length = len(data_2)
                index = 1
                surplus_principal = 0
                surplus_interest = 0
                next_plan_id = None
                if length <= 1:
                    continue
                for it2 in data_2:
                    params = []
                    if index == 1:
                        params.append(it2["ID"])
                        params.append(None)
                        params.append(surplus_principal)
                        params.append(surplus_interest)
                    else:
                        params.append(it2["ID"])
                        params.append(data_2[index-2]["ID"])
                        params.append(surplus_principal)
                        params.append(surplus_interest)

                    index += 1
                    surplus_principal = surplus_principal + round((it2["AMOUNT"]) * 100)
                    surplus_interest = surplus_interest + round(it2["INTEREST"] * 100)
                    args.append(params)
            row = jdbc_util.sql_execute(temp_sql, "insert", connect=int_connect, args=args)
            print("{} SQL执行条数：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), row))
            row_total -= 1000 if row_total > 1000 else row_total
            print("{} 待执行条数：{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), row_total))
            print("**************************************************************************")
        page_curr += 1
        page_total -= 1

# 关闭连接
if out_connect:
    jdbc_util.sql_close(db_output_source["db_type"], out_connect)
    print("{} [数据库连接关闭] {} connect to close.".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                     db_output_source["db_type"]))

if int_connect:
    jdbc_util.sql_close(db_input_source["db_type"], int_connect)
    print("{} [数据库连接关闭] {} connect to close.".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                                     db_input_source["db_type"]))
print("==============================================================================")

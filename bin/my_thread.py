#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- 
# -- 
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Email lmay@lmaye.com
# Date: 
# ----------------------------------------------------------
from time import sleep
from PyQt5.QtCore import QThread, pyqtSignal


class MyThread(QThread):
    sinOut = pyqtSignal(str)

    def __init__(self, parent=None):
        super(MyThread, self).__init__(parent)
        self.threadName = "MyThread"
        self.times = 10

    def setIdentity(self, val):
        self.threadName = val

    def run(self):
        while self.times > 0:
            sleep(2)
            # 发射信号
            self.sinOut.emit("[{}] 发射信号 ==> {}".format(self.threadName, self.times))
            self.times -= 1

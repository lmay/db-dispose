#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------
# -- 
# -- 
# ****************************
# Author: lmay.Zhou
# Blog: www.lmaye.com
# Email lmay@lmaye.com
# Date: 
# ----------------------------------------------------------
import sys
from PyQt5.QtWidgets import (QWidget, QVBoxLayout, QApplication, QTextEdit, QPushButton, QListWidget)

from bin.my_thread import MyThread


class Example(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.txt = QListWidget(self)
        button = QPushButton("执 行")

        vbox = QVBoxLayout()
        vbox.addWidget(self.txt)
        vbox.addWidget(button)

        button.clicked.connect(self.execute)
        self.setLayout(vbox)
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Signal & slot')
        self.show()

    def outText(self, text):
        self.txt.addItem(text)

    def execute(self):
        # 创建一个线程实例并设置名称、变量、信号与槽
        self.thread = MyThread()
        self.thread.setIdentity("LogThread")
        self.thread.sinOut.connect(self.outText)
        self.thread.start()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
